#Checkers Game

---

#Author

Lucas Kendi Fattore Hirano

---

#Files list:
README.md

index.html

cell.js

checkers.js

style.css



---

#How to run?

Download the files into a folder and run index.html on the browser.

---

#What does it do?
It's a two players checkers game

It follows the same classic checkers rules

The player with the red pieces starts

Players can only move diagonally forwards, one square per turn

If there is an adversary piece aligned with a empty space, one player can jump the other player's piece and remove it fronm the board

If one player manage to move one piece to the opposite side of the board, the piece becomes a king and can move backwards

The game ends when one player have no pieces on the board

