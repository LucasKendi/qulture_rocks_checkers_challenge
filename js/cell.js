function Cell(x, y, color, type, status){
	this.x = x;
	this.y = y;
	this.color = color;
	this.type = type;
	this.status = status;

	this.clear = function(){
		this.color = "black";
		this.type = "free";
		this.status = "ready";
	}

	this.fill = function(cell){
		this.color = cell.color;
		this.type = cell.type;
		this.status = "ready";
	}

	this.click = function(){
		if(this.status == "ready"){
			this.status = "selected";
		}
		else{
			this.status = "ready";
		}
	}

	this.turnKing = function(){
		this.type = "king";
	}

	this.drawPiece = function(){
		if(this.type != "free"){
			ctx.beginPath();
			ctx.arc(x+30, y+30, 25, 0, 2*Math.PI);
			ctx.fillStyle = this.color;
			ctx.fill();

			if(this.status == "selected"){
				ctx.strokeStyle = "LawnGreen";
				ctx.lineWidth = 5;
				ctx.stroke();
			}
			if(this.type == "king"){
				ctx.beginPath();
				ctx.arc(x+30, y+30, 18, 0, 2*Math.PI);
				ctx.fillStyle = 'rgba(255,215,0,0.7)';
				ctx.fill();
			}

		}
	}

	this.doMove = function(i, j, turn, board){
		if(this.canMove(i,j,turn, board)){
			var moveTo = board[i][j];
			var eaten = board[parseInt((this.x+moveTo.x)/120)][parseInt((this.y+moveTo.y)/120)]
			if(eaten == this || eaten == moveTo){eaten = null}
			moveTo.fill(this);
			this.clear();
			if(eaten){
				turn = eaten.color;
				eaten.clear()
			}
			if(moveTo.y == 0 && moveTo.color == "white"){
				moveTo.turnKing();
			}
			else if(moveTo.y == 540 && moveTo.color == "red"){
				moveTo.turnKing();
			}
			if(moveTo.color == "red"){
				return "white";
			}
			else{
				return "red";
			}
		}
		else{
			return this.color;
		}
	}

	this.canMove = function(i, j, turn, board){
		var answer = false;
		if(turn == this.color){
			var moveTo  = board[i][j];
			var eaten = board[parseInt((this.x+moveTo.x)/120)][parseInt((this.y+moveTo.y)/120)];
			if(moveTo.type == "free"){
				var xMove = (moveTo.x - this.x)/60;
				var yMove = (moveTo.y - this.y)/60;
				if(Math.abs(xMove) == Math.abs(yMove)){
					if(this.color == "white"){
						switch (true){
							case yMove == -1 || (yMove == -2 && this.color != eaten.color && eaten.type != "free"):
								answer = true;
								break;
							case (yMove == 1 && this.type == "king") || (yMove == 2 && this.type == "king" && this.color != eaten.color && eaten.type != "free"):
								answer = true;
								break;
						}
					}
					else{
						switch (true){
							case yMove == 1 || (yMove == 2 && this.color != eaten.color && eaten.type != "free"):
								answer = true;
								break;
							case (yMove == -1 && this.type == "king") || (yMove == -2 && this.type == "king" && this.color != eaten.color && eaten.type != "free"):
								answer = true;
								break;
						}
					}
				}
			}
		}
	return answer;
	}
}