var canvas = document.getElementById('gameCanvas');
var ctx = canvas.getContext('2d');
var cols = 10;
var rows = 10;
var selected;
var clicked;
var turn = "red";

function createBoard(){
	var board = new Array(cols);	
	for(i = 0; i < cols; i++){
		board[i] = new Array(rows);
	}
	return board;
}

var board = createBoard();
initGame();
updateGame();

//Setup of board entities
function initGame(){
	for(var i = 0; i < cols; i++){
		for(var j = 0; j < rows; j++){

			var x = i * 60;
			var y = j * 60;

			if((i+j) % 2 == 1){
				if(j < 3){
					board[i][j] = new Cell(x, y, "red", "men", "ready");
				}
				else if(j > 6){
					board[i][j] = new Cell(x, y, "white", "men", "ready");
				}
				else{
					board[i][j] = new Cell(x, y,"black", "free", "ready");	
				}
				
			}
		}
	}
}

function gameEnd(board){
	var redPieces = 0;
	var whitePieces = 0;
	for(var i = 0; i < cols; i++){
		for(var j = 0; j < rows; j++){
			if(board[i][j]){
				currentColor = board[i][j].color;
				if(currentColor == "red"){
					redPieces++;
				}
				else if(currentColor == "white"){
					whitePieces++;
				}
			}
		}
	}
	if(redPieces == 0){
		alert("White is the winner!");
		return true;
	}
	else if(whitePieces == 0){
		alert("Red is the winner!");
		return true;
	}
	return false;
}

//Redraws the board and pieces using current board's data, updating the game
function updateGame(){
	for(var i = 0; i < cols; i++){
		for(var j = 0; j < rows; j++){
			var x = i * 60;
			var y = j * 60;
			cell = board[i][j];
			drawSquare(i, j, x, y);
			if(cell){
				cell.drawPiece();
			}
		}
	}
	gameEnd(board);
}

//Draws the squares following a "every other" ruleset
function drawSquare(i, j, x, y){
	if((i+j) % 2 == 0){
		ctx.fillStyle = 'beige';
		ctx.fillRect(x, y, 60, 60);
	}
	else{
		ctx.fillStyle = 'DimGrey';
		ctx.fillRect(x, y, 60, 60);			
	}
}

//Check clicked square
canvas.onclick = function(event){
	x = event.clientX-8;
	y = event.clientY-8;
	clicked = board[parseInt(x/60)][parseInt(y/60)];

	for(var i = 0; i < cols; i++){
		for(var j = 0; j < rows; j++){
			var cell = board[i][j];
			if(cell){
				if(cell.status == "selected" && cell != clicked){
					selected = cell;
					cell.status = "ready";
				}
			}
		}
	}

	if(clicked){
		if(clicked.type == "free" && selected){
			turn = selected.doMove(parseInt(x/60), parseInt(y/60), turn, board);
			selected = null;
		}
		else if(clicked.color == turn){
			clicked.click();
		}
	}
	updateGame();
}

function passTurn(){
	if(turn == "red"){
		turn = "white";
	}
	else{
		turn = "red";
	}
}