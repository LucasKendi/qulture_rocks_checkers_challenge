QUnit.test("Board should exist", function(){
	var board = createBoard();
	ok(board !== null);
});

QUnit.test("Cells should be initiated", function(assert){

	var cell = new Cell(60,120,"red","men","ready");

	assert.equal(cell.x, 60);
	assert.equal(cell.y, 120);
	assert.equal(cell.color, "red");
	assert.equal(cell.type, "men");
	assert.equal(cell.status, "ready");
	
});

QUnit.test("Cells should be cleared", function(assert){

	var cell = new Cell(60,120,"red","men","ready");
	cell.clear();

	assert.equal(cell.x, 60);
	assert.equal(cell.y, 120);
	assert.equal(cell.color, "black");
	assert.equal(cell.type, "free");
	assert.equal(cell.status, "ready");
	
});

QUnit.test("Cells should be filled", function(assert){

	var cell = new Cell(60,120,"black","free","ready");
	var cellFill = new Cell(120,180,"white","king","ready");
	cell.fill(cellFill);

	assert.equal(cell.x, 60);
	assert.equal(cell.y, 120);
	assert.equal(cell.color, "white");
	assert.equal(cell.type, "king");
	assert.equal(cell.status, "ready");
	
});

QUnit.test("Cells should be clicked", function(assert){

	var cell = new Cell(60,120,"red","men","ready");
	cell.click();

	assert.equal(cell.x, 60);
	assert.equal(cell.y, 120);
	assert.equal(cell.color, "red");
	assert.equal(cell.type, "men");
	assert.equal(cell.status, "selected");
	
});

QUnit.test("Cells should turn king", function(assert){

	var cell = new Cell(60,120,"red","men","ready");
	cell.turnKing();
	assert.equal(cell.x, 60);
	assert.equal(cell.y, 120);
	assert.equal(cell.color, "red");
	assert.equal(cell.type, "king");
	assert.equal(cell.status, "ready");
	
});

QUnit.test("Cell should be able to move", function(){


	var board = createBoard();
	initGame();
	var cell = new Cell(60,120,"red","men","ready");	
	var cellMove = new Cell(120,180,"black","free","ready");
	var cellNotMove = new Cell(0,180,"red","men","ready");	
	board[1][2] = cell;
	board[2][3] = cellMove;
	board[0][3] = cellNotMove;

	ok(cell.canMove(2, 3, "red", board));
	ok(!(cell.canMove(0, 3, "red", board)));
});


QUnit.test("Cell should do move", function(assert){


	var board = createBoard();
	initGame();
	var cell = new Cell(60,120,"red","men","ready");	
	var cellMove = new Cell(120,180,"black","free","ready");
	board[1][2] = cell;
	board[2][3] = cellMove;
	
	assert.equal(cell.doMove(2, 3, "red", board), "white");
});

QUnit.test("Cell should do move", function(assert){


	var board = createBoard();
	initGame();
	var cell = new Cell(60,120,"red","men","ready");	
	var cellNotMove = new Cell(0,180,"red","men","ready");	
	board[1][2] = cell;
	board[0][3] = cellNotMove;
	
	assert.equal(cell.doMove(0, 3, "red", board), "red");
});


QUnit.test("Turn should be passed", function(assert){
	turn = "red";

	passTurn();
	assert.equal(turn, "white");

	passTurn();
	assert.equal(turn, "red");
	
});


QUnit.test("Game should be initiated", function(){
	var board = createBoard();
	initGame();
	ok(board[0][0] !== null);
	
});

QUnit.test("Game should not end", function(){
	var board = createBoard();
	initGame();
	board[0][1] = new Cell(0, 60, "red", "men", "ready");
	board[0][7] = new Cell(0, 60, "white", "men", "ready");
	ok(gameEnd(board) == false);
});

QUnit.test("Game should end", function(){
	var board = createBoard();
	initGame();
	board[0][1] = new Cell(0, 60, "red", "men", "ready");
	board[0][7] = new Cell(0, 60, "red", "men", "ready");
	ok(gameEnd(board) == true);
});